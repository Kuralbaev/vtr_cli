#!/usr/bin/env node

const ComponentCreate = require('./utilits/create/component')
const PageCreate = require('./utilits/create/page')
const UpdateDefaultRouter = require('./utilits/update/router')
const UpdateDefaultVuex = require('./utilits/update/vuex')
const UpdateDefaultStyle = require('./utilits/update/style')

const commander = require('commander')
const chalk = require('chalk')

commander.version('1.0.0').description('Configuration files creator.')

commander
  .command('create <name>')
  .option('-c', 'Create component')
  .description('Create new page')
  .action(async (name, cmd) => {
    if (cmd.c) {
      await new ComponentCreate(name).create()
    } else {
      await new PageCreate(name).create()
    }
  })

commander
  .command('default')
  .option('--all', 'Default update all')
  .option('-r', 'Default router')
  .option('-s', 'Default style')
  .option('-v', 'Default vuex')
  .description('Update default [-r] router || [-s] vuex')
  .action(async (cmd) => {
    if (cmd.r) {
      await UpdateDefaultRouter.create()
    }
    if (cmd.v) {
      await UpdateDefaultVuex.create()
    }
    if (cmd.s) {
      await UpdateDefaultStyle.create()
    }

    if (cmd.all) {
      await UpdateDefaultRouter.create()
      await UpdateDefaultVuex.create()
      await UpdateDefaultStyle.create()
    }

    if (!Object.keys(cmd).length)
      console.log(
        chalk.yellow(
          `Options: [-r] router | [-v] vuex | [-s] style | [--all] all default`
        )
      )
  })

commander.parse(process.argv)
