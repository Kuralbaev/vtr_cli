const path = require('path')

module.exports = class Helpers {
  static parsePath(path) {
    if (!path.toString().includes('/'))
      return {
        path: '',
        importName: path,
        name: path,
      }

    let _path = path.split('/').map((e) => e.toString().capitalize())
    let refName = _path.splice(-1, 1)

    return {
      path: '/' + _path.join('/'),
      importName: _path.join('') + refName,
      name: refName,
    }
  }

  static rootPath() {
    let _path = path.dirname(__dirname).split(path.sep)
    _path.splice(-2, 2)

    return _path.join('/')
  }
}
