const fs = require('fs')
const shell = require('shelljs')
const chalk = require('chalk')
const Helpers = require('../../helpers/Helpers')

const _routes = `//key_import

const routes = [
  //key_route
]

export default routes
`

const _index = `import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes.js'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
`

module.exports = class UpdateDefaultRouter {
  static async create() {
    await shell.mkdir('-p', Helpers.rootPath() + '/src/router')

    await fs.writeFileSync(Helpers.rootPath() + `/src/router/index.js`, _index)
    await fs.writeFileSync(Helpers.rootPath() + `/src/router/routes.js`, _routes)

    console.log(chalk.green(`\nCreate default router`))
  }
}
