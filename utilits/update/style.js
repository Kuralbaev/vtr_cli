const fs = require('fs')
const shell = require('shelljs')
const chalk = require('chalk')
const Helpers = require('../../helpers/Helpers')

const _style = `//ColorVariables
@import "colors";

//Elements
@import "other";

*{
  box-sizing: border-box;

  &:focus{
    outline: none;
  }
}`

module.exports = class UpdateDefaultStyle {
  static async create() {
    await shell.mkdir('-p', Helpers.rootPath() + '/src/style')

    await fs.writeFileSync(Helpers.rootPath() + `/src/style/colors.scss`, '')
    await fs.writeFileSync(Helpers.rootPath() + `/src/style/_other.scss`, '')
    await fs.writeFileSync(Helpers.rootPath() + `/src/style/index.scss`, _style)

    console.log(chalk.green(`\nCreate default style`))
  }
}
