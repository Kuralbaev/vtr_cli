const fs = require('fs')
const shell = require('shelljs')
const chalk = require('chalk')
const Helpers = require('../../helpers/Helpers')

const _auto = `const requireModule = require.context('.', true, /\.js$/)
const modules = {}

requireModule.keys().forEach(filename => {
  if (filename === './_auto.js' || filename.includes('useState')) return

  const moduleName = filename
    .replace(/(\\.\\/|\\.js)/g, '')
    .replace(/^[^/]+\\//, '')
    .replace(/^\\w/, c => c.toUpperCase())

  modules[moduleName] =
    requireModule(filename).default || requireModule(filename)

  modules[moduleName].namespaced = true
})

export default modules
`

const _index = `import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import modules from './modules/_auto'

const store = new Vuex.Store({
  modules: modules
})

export default store
`

module.exports = class UpdateDefaultVuex {
  static async create() {
    await shell.mkdir('-p', Helpers.rootPath() + '/src/store')
    await shell.mkdir('-p', Helpers.rootPath() + '/src/store/modules')

    await fs.writeFileSync(Helpers.rootPath() + `/src/store/index.js`, _index)
    await fs.writeFileSync(
      Helpers.rootPath() + `/src/store/modules/_auto.js`,
      _auto
    )

    console.log(chalk.green(`\nCreate default vuex`))
  }
}
