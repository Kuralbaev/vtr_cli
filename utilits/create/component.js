const fs = require('fs')
const path = require('path')
const chalk = require('chalk')

const _template = `<template>
  <div>
    {{ msg }}
  </div>
</template>

export default {
  data(){
    msg: 'Pages'
  }
}`

module.exports = class ComponentCreate {
  constructor(name) {
    this.name = name
  }

  async create() {
    const _path = Helpers.rootPath() + '/src/components'

    if (!fs.existsSync(_path)) {
      fs.mkdirSync(_path)
    }

    await fs.writeFileSync(`${_path}/${this.name}.vue`, _template)

    console.log(chalk.green(`\nComponent "${name}" created`))
  }
}
