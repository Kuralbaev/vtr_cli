const fs = require('fs')
const path = require('path')
const shell = require('shelljs')
const Helpers = require('../../helpers/Helpers')

const _template = (name) => `const state = {
  all: [],
  detail: {}, 
  isLoading: false, // Loader страницы
  error: {} // Все Errors который получем из Server-а
}

// Используем что-бы не конфликтовал глобальные Actions, Mutations и Getters
export const mutationTypes = {
  Start: '[${name}] Start',
  Success: '[${name}] Success',
  SuccessDetail: '[${name}] SuccessDetail',
  Failure: '[${name}] Failure'
}

export const actionTypes = {
  actionMethod: '[${name}] actionMethod'
}

export const getterTypes = {
  all: '[${name}] all',
  detail: '[${name}] detail',
  isLoading: '[${name}] isLoading',
  error: '[${name}] error'
}

const mutations = {
  [mutationTypes.Start](state, payload) {
    state.isLoading = true
    state.all = []
  },
  [mutationTypes.getTestData](state, payload) {
    state.isLoading = false
    state.all = payload
  },
  [mutationTypes.Failure](state, payload) {
    state.isLoading = false
    state.error = payload
  }
}

const actions = {
  [actionTypes.actionMethod]({ commit }, path) {
    // Используем Promise когда ждем ответа от Сервера
    return new Promise(resolve => {
      commit(mutationTypes.getTestData, path)

      resolve()
    })
  }
}


const getters = {
  [getterTypes.all]: state => state.all,
  [getterTypes.detail]: state => state.detail,
  [getterTypes.isLoading]: state => state.isLoading,
  [getterTypes.error]: state => state.error,
}

export default { state, mutations, actions, getters }`

module.exports = class StoreCreate {
  constructor(path) {
    this.pathName = path
  }

  async create() {
    const _path = Helpers.rootPath() + '/src/store/modules'

    if (!fs.existsSync(_path)) {
      shell.mkdir('-p', _path)
    }

    await fs.writeFileSync(
      `${_path}/${this.pathName.importName}.store.js`,
      _template(this.pathName.importName)
    )
  }
}
