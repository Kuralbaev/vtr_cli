const fs = require('fs')
const chalk = require('chalk')
const StoreCreate = require('./store')
const RouterCreate = require('./router')
const shell = require('shelljs')
const Helpers = require('../../helpers/Helpers')

const _template = ({ name, importName }) => `<template>
  <div>
    {{ msg }}
  </div>
</template>

<script>
import { mapGetters } from 'vuex'
import { getterTypes, actionTypes } from '@/store/modules/${importName}'

export default {
  name: '${importName}',
  data(){
    return {
      msg: '${name}'
    }
  },
  computed: {
    ...mapGetters({
      user: getterTypes.all,
    })
  },
  async mounted() {
    await this.$store.dispatch(actionTypes.actionMethod)
  }
}
</script>
`

String.prototype.capitalize = function () {
  return this.charAt(0).toUpperCase() + this.slice(1)
}

module.exports = class PageCreate {
  constructor(name) {
    this.name = name
  }

  async create() {
    let _parse = Helpers.parsePath(this.name)
    let _path = Helpers.rootPath() + '/src/views' + _parse.path

    if (!fs.existsSync(_path)) {
      shell.mkdir('-p', _path)
    }

    await fs.writeFileSync(`${_path}/${_parse.name}.vue`, _template(_parse))

    await new StoreCreate(_parse).create()
    await new RouterCreate(_parse).create()

    console.log(chalk.green(`\nPage "${_parse.name}" created`))
  }
}
