const fs = require('fs')
const Helpers = require('../../helpers/Helpers')
const shell = require('shelljs')

const _importFile = ({
  name,
  importName,
  path,
}) => `import ${importName} from '@/pages${path}/${name}'
//key_import`

const _template = ({ name, path, importName }) => `{
    path: '${path.toString().toLowerCase()}/${name.toString().toLowerCase()}',
    name: '${importName}',
    component: ${importName}
  },
  //key_route`

module.exports = class RouterCreate {
  _path = Helpers.rootPath() + '/src/router'

  constructor(path) {
    this.pathName = path
  }

  async open() {
    const _file = fs.readFileSync(this._path + '/routes.js', 'utf8')

    return _file
  }

  async create() {
    let _routes = await this.open()

    _routes = await _routes.replace('//key_import', _importFile(this.pathName))
    _routes = await _routes.replace('//key_route', _template(this.pathName))

    await fs.writeFileSync(`${this._path}/routes.js`, _routes)
  }
}
